% Objets Government has predicted
object(balloon).
object(clothesline).
object(frisbee).
object(water_tower).

% Days that objects were seen during the week
day(tuesday).
day(wednesday).
day(thursday).
day(friday).

% Different day variations
earlier(tuesday, wednesday).
earlier(tuesday, thursday).
earlier(tuesday, friday).

earlier(wednesday, thursday).
earlier(wednesday, friday).

earlier(thursday, friday).

later(friday, thursday).
later(friday, wednesday).
later(friday, tuesday).

later(thursday, wednesday).
later(thursday, tuesday).

later(wednesday, tuesday).

solve :-
    object(BarradaObject),
    object(GortObject),
    object(KlatuObject),
    object(NiktoObject),
    all_different([BarradaObject, GortObject, KlatuObject, NiktoObject]),

    day(BarradaDay),
    day(GortDay),
    day(KlatuDay),
    day(NiktoDay),
    all_different([BarradaDay, GortDay, KlatuDay, NiktoDay]),

    People = [ [ms_barrada, BarradaObject, BarradaDay],
               [ms_gort, GortObject, GortDay],
               [mr_klatu, KlatuObject, KlatuDay],
               [mr_nikto, NiktoObject, NiktoDay] ],

    


    % 1. Mr. Klatu made his sighting at some point earlier in the week than the one who saw the balloon,
	% but at some point later in the week than the one who spotted the frisbee (who isnt Ms. Gort).

    get_info(People, [_, balloon, BallonDay]),
    get_info(People, [_, frisbee, FrisbeeDay]),
    earlier(KlatuDay, BallonDay),
    later(KlatuDay, FrisbeeDay),
    \+ member([ms_gort, frisbee, _], People),

    % 2 Fridays sighting was made by either Ms. Barrada or the one who saw a clothesline (or both).
    ( member([ms_barrada, _, friday], People);
      member([_, clothesline, friday], People);
      member([ms_barrada, clothesline, friday], People)),

    % 3 Mr. Nikto did not make his sighting on Tuesday.
    \+ member([mr_nikto, _, tuesday], People),

    % 4. Mr. Klatu is not the one whose object turned out to be a water tower.
    \+ member([mr_klatu, water_tower, _], People),

    tell(ms_barrada, BarradaObject, BarradaDay),
    tell(ms_gort, GortObject, GortDay),
    tell(mr_klatu, KlatuObject, KlatuDay),
    tell(mr_nikto, NiktoObject, NiktoDay).

    % Success  all sighted are Different
    % Fail  it is unsighted or equal
    all_different([H | T]) :- member(H, T), !, fail.
    all_different([_ | T]) :- all_different(T).
    all_different([_]).

    % Use info to find UFO sighting
    get_info([X|_], X).
    get_info([_|T], X) :- get_info(T, X).

    tell(X, Y, Z) :- write(X), write(' saw a '), write(Y), write(' on '), write(Z), nl.